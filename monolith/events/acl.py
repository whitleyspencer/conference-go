import requests
from .keys import pexel_api, open_weather_api
import json


def get_photo(city, state):
    """
    inputs: city and state from request body/content dict
    returns: picture URL
    """
    request_url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {"Authorization": pexel_api}
    response = requests.get(request_url, headers=headers)
    data = json.loads(response.content)
    photo_url = data["photos"][0]["src"]["original"]
    return photo_url


"""
--> get_weather_data is called in GET request to ConfDetail
    --> conference's location-- city, and state abbreviation are passed in as arguments
    --> passes these^ + country code, api key, as url parameters in request to GeoCode
        --> accesses "lat" and "lon" coordinates
            --> uses these^ in request to OpenWeather
                --> accesses "temp" and "description"
                    --> returns these^ in a dictionary
--> JsonResponse of GET request should also include: "weather": {return value of get_weather_data}

"""


def get_coordinates(city, state):
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={open_weather_api}"
    geocode_response = requests.get(geocode_url)
    geocode_data = json.loads(geocode_response.content)
    return {"lat": geocode_data[0]["lat"], "lon": geocode_data[0]["lon"]}


def get_weather_data(city, state):
    coordinates = get_coordinates(city=city, state=state)
    lat, lon = coordinates["lat"], coordinates["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={open_weather_api}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.content)
    description = weather_data["weather"][0]["description"]
    temp = weather_data["main"]["temp"]

    return {"temp": temp, "description": description}
