import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    # adding response from conference list GET request to content dict
    content = json.loads(response.content)
    # for each conference in the list of conferences,
    for conference in content["conferences"]:
        # create/update an instance of ConferenceVO
        #   with name and href of conference
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
